﻿// Maszyna Turinga.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>

using namespace std;

class Data {
public:

    string tape;
    int state = 0;
    string q[7*3] ={"1BP", "5BP", "---",
                    "10P", "21P", "---", 
                    "31L", "21P", "4BL",
                    "30L", "31L", "0BP",
                    "40L", "4BL", "60P",
                    "5BP", "5BP", "6BP",
                    "---", "---", "---"};

    Data()
    {   
        string helloMessage = {
            "Program symulujacy dzialanie maszyny Turinga \n"
            "opis MT \n"
            "M = ( {q0, q1, q2, q3, q4, q5, q6}, {0, 1}, {0, 1, B}, \xeb, q0, B, 0 )\n\n"
        };
        cout << helloMessage;
    }

    void printState(int index)
    {   
        string textTape;
        string textState = "[q" + to_string(state) + "]";
        
        bool beforeDigit = true;

        // DEBUG
        cout << " index " << index << " len: " << tape.length()-1 << " tape " << tape << " ";

        for (int i = tape.length()-1; i >= 0; i--)
        {
            
            if (i == index)
            {
                textTape = textState + tape[i] + textTape;
                if (tape[i] != 'B' && i > 0) beforeDigit = false;
                continue;
            }

            if (tape[i] == 'B' && beforeDigit && i > 0) continue;
            else beforeDigit = false;

            textTape = tape[i] + textTape;

            
        }
        cout << textTape << endl;
    }

    void tapeResult()
    {
        int result = 0;

        for (int i = 0; i < tape.length(); i++)
        {
            if (tape[i] == '0') result++;
        }

        cout << "Wynik dzialania maszyny to: " << result << endl; 
    }
};



int changeState(Data& data, int index)
{   
    int actTapeData = data.tape[index] - '0';
    if (actTapeData > 2) actTapeData = 2;
    actTapeData += data.state * 3;
    int newState = 0;
    int tapeDir = 0;

    newState = data.q[actTapeData][0] - '0';
    

    data.tape[index] = data.q[actTapeData][1];

    if (data.q[actTapeData][2] == 'P') tapeDir = 1;
    else if (data.q[actTapeData][2] == 'L') tapeDir = -1;
    
    data.state = newState;

    return tapeDir;
 
}

int main()
{   
    Data data;
    int stateIndex = 0;

    // Read from the text file
    cout << "Podaj nazwe pliku z tasma (0 for default) : ";
    string fileName;
    cin >> fileName;

    if (fileName == "0") fileName = "tasma.txt";

    ifstream File;
    File.open(fileName);
    if (File) {
        cout << "Otwarto plik: " << fileName << endl;
    }
    else {
        cout << "Plik " << fileName << " nie istnieje";
        return 1;
    }

    // Use a while loop together with the getline() function to read the file line by line
    while (getline(File, data.tape)) {
        
    }

    File.close();

    while (data.state != 6)
    {
        data.printState(stateIndex);
        stateIndex += changeState(data, stateIndex);

    }

    
       
    data.printState(stateIndex);

    data.tapeResult();

    return 0;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
